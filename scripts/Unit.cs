using Godot;
using System;
using System.Collections.Generic;

/// <summary>
/// A Unit is a playable character
/// </summary>
public class Unit : EntityPhysical
{
	private int _defense; public int Defense { get => _defense; }
	private Shape _movement;
	private Datasheet _dataSheet; public Datasheet GetDataSheet { get => _dataSheet; }
	private List<Attack> _attackList;   public List<Attack> GetAttackList { get => _attackList; }
	private List<Passive> _passiveList; public List<Passive> GetPassiveList { get => _passiveList; }
	private Tile _currentTile;  public Tile GetCurrentTile { get => _currentTile; }
	private List<String> _blockedBy = new List<String>();
	private Renderer _renderer;
	private List<Tile> _range = new List<Tile>();
	private bool _movedThisTurn = false; public bool MovedThisTurn { get => _movedThisTurn; set => _movedThisTurn = value; }
	private TextRenderer _textRenderer; public TextRenderer GetTextRenderer { get => _textRenderer; }

	private List<Action> _targetedByActionList = new List<Action>(); public List<Action> TargetedByActionList { get => _targetedByActionList; }
	private List<Action> _blocksActionList = new List<Action>(); public List<Action> BlocksActionList {get => _blocksActionList; }
	//The list of units that exists, use GetUnitBoardList to get the units that are under a player control.
	private static List<Unit> _unitList = new List<Unit>(); public static List<Unit> GetUnitList { get => _unitList; }

	//This is the list of the units present on the board
	private static List<Unit> _unitBoardList = new List<Unit>(); public static List<Unit> GetUnitBoardList { get => _unitBoardList; }

	/// <summary>
	/// Creates a unit that can be placed on the board, use unit.SetCurrentTile(Tile) to place it on the board
	/// </summary>
	public Unit (Unit unit, ETeam team) : base(unit, team) {
		_defense = unit._defense;
		_movement = unit._movement;
		_attackList = new List<Attack>();
		foreach (Attack a in unit._attackList) {
			_attackList.Add(new Attack(a, this));
		}
		_passiveList = new List<Passive>();
		foreach (Passive p in unit._passiveList) {
			_passiveList.Add(p.Clone(this));
		}
		_renderer = new SpriteRenderer((SpriteRenderer)unit._renderer, "idle");
		_dataSheet = new Datasheet();
		_blockedBy = new List<String>(unit._blockedBy);
		_textRenderer = new RichTextRenderer((RichTextRenderer)unit._textRenderer, this, EntityBuilder.GetSceneNode);

		AddChild(_renderer);

		Name = "UNIT";
	}

	/// <summary>
	/// A Unit is a playable character
	/// </summary>
	/// <param name="title"> the name of the unit </param>
	/// <param name="description"> the description of the unit </param>
	/// <param name="filter"> a filter list used to check if a unit can block a target</param>
	/// <param name="team"> the team of the unit</param>
	/// <param name="defense"> the defense points of the unit </param>
	/// <param name="movement"> the movement points of the unit </param>
	/// <param name="tags"> the tags describing the unit, will be used by <see cref="EntityFilter"/> to check if that unit is targetable</param>
	/// <param name="attackList"> a list of attacks that this unit can use</param>
	/// <param name="pasiveList"> a list of passives that this unit can use</param>
	/// <param name="renderer"> the Renderer instance related to this unit: <see cref="EntityBuilder.buildSingleUnit(string, ETeam)"/></param>
	public Unit (String title, String description, ETeam team, int defense, Shape movement, List<String> tag, List<Attack> attackList, List<Passive> passiveList, Renderer renderer, List<String> blockedBy, TextRenderer textRenderer) 
	: base (title, description, team, tag) {
		AddChild(renderer);

		_defense = defense;
		_movement = movement;
		_attackList = attackList;
		_passiveList = passiveList;
		_renderer = renderer;
		_dataSheet = new Datasheet();
		_blockedBy = blockedBy;
		_textRenderer = textRenderer;

		_attackList.Sort((y, x)=>x.GetDamage.CompareTo(y.GetDamage));

		Name = "UNIT";

		_unitList.Add(this);
	}

	public bool SetCurrentTile (Tile tile) {
		if (tile.unitPresent() || BlockedBy(tile)) {
			return false;
		}
		if (_currentTile != null) {
			_currentTile.SetLocalUnit(null);
		} else {
			_unitBoardList.Add(this); //If the unit wasnt on the board, we add it to the list
		}
		_currentTile = tile;
		_currentTile.SetLocalUnit(this);
		Translation = Vector3.Zero;
		return true;
	}

	public void Skin (int team) {
		_renderer.Skin(team);
	}

	public void IsAttacked () {

		
	}

	/// <summary>
	/// This evaluates an unit against another, it takes the attacks from best to worst, and tries to find one that can hit the target.
	/// Returns a positive int if it deals damage, a negative int if it protects, and 0 if it does nothing.
	/// </summary>
	public int EvaluateAttacksOn (Unit u) {
		if (_dataSheet.Surrend)
			return 0; //You cannot act if you surrend
		if (u == this)
			return -_defense;
		foreach (Attack a in _attackList) {
			//We make sure that the attack is baked, and we get the list of units that can be hit by this attack
			if (a.SelectRange().Contains(u))
				if (u.IsAliedTo(this))
					if (u.GetDataSheet.Surrend)
						return 0; //You cannot help a unit that surrenders
 					else
						return -(_defense + _dataSheet.Defense);
				else
					return a.GetDamage + _dataSheet.Damage;
		}
		return 0;
	}

	public bool BlockedBy (Tile tile) {
		return BlockedBy(tile.GetLocalTerrain) || BlockedBy(tile.GetLocalUnit);
	}

	public bool BlockedBy (EntityPhysical entity) {
		if (entity == null)
			return false;
		
		if (GetTeam == entity.GetTeam)
			return false;



		foreach (String filter in _blockedBy) {
			foreach (String tag in entity.GetTagList) {
				System.Console.WriteLine("trying " + filter + ".Equals(" + tag + ")");
				return filter.Equals(tag, StringComparison.OrdinalIgnoreCase);
			}

		}

		return false;
	}

	///<summary>
	/// Highlights and returns a list of unit that the unit can attack
	///</summary>
	public List<Unit> AttackRange () {
		if (_attackList.Count == 0)
			return new List<Unit>();
		if (_dataSheet.Surrend)
			return new List<Unit>(); //You cannot attack if you surrend
		List<Unit> tmp = _attackList[0]?.SelectRange();
		foreach (Unit u in tmp) {
			if (!u.IsAliedTo(this)) {
				Tile.HighlightTile(u.GetCurrentTile, Tile.EEffectType.Target);
				u.GetTextRenderer.Print(""+_attackList[0]?.GetDamage, TextRenderer.ETextRenderType.Danger);
			}
		}
		return tmp;
	}

	public void TileRange () {
		if (_attackList.Count == 0)
			return;
		Tile.HighlightTiles(_attackList[0]?.TileRange(), Tile.EEffectType.Range);
	}

	public int Hits (Unit u) {
		if (_dataSheet.Surrend)
			return 0; //You cannot hit if you surrend
		foreach (Attack a in _attackList) {
			if (a.Hits(u))
				return a.GetDamage + _dataSheet.Damage;
		}
		return 0;
	}

	public int Helps (Unit u) {
		if (_dataSheet.Surrend)
			return 0; //You cannot help if you surrend
		if (u.GetDataSheet.Surrend)
			return 0; //You cannot help a unit that surrenders
		foreach (Attack a in _attackList) {
			if (a.Helps(u))
				return _defense + _dataSheet.Defense;
		}
		return 0;
	}

	///<summary>
	/// Highlights and returns a list of unit that the unit can help
	///</summary>
	public List<Unit> HelpRange () {
		if (_attackList.Count == 0)
			return new List<Unit>();
		if (_dataSheet.Surrend)
			return new List<Unit>(); //You cannot help if you surrend
		List<Unit> tmp = _attackList[0]?.SelectRange();
		foreach (Unit u in tmp) {
			if (u.IsAliedTo(this)) {
				if (!u.GetDataSheet.Surrend) { //You can only help unit that havent surrendered
					Tile.HighlightTile(u.GetCurrentTile, Tile.EEffectType.Shield);
					u.GetTextRenderer.Print(""+_defense, TextRenderer.ETextRenderType.Support);
				}
			}
		}
		return tmp;
	}

	///<summary>
	/// Highlights and returns a list of tile that the unit can move to
	///</summary>
	public List<Tile> MovingRange () {
		if (_dataSheet.Surrend)
			return new List<Tile>(); //You cannot move if you surrend
		Tile.RemoveAllHighlightedTiles();
		List<Tile> tmp = _movement.SelectMovementOf(this, _dataSheet.Movement);
		_range = tmp;
		Tile.HighlightTiles(tmp, Tile.EEffectType.Movement);
		return tmp;
	}

	///<summary>
	/// Highlights and returns a list of unit that can help this unit
	///</summary>
	public List<Unit> HelpedByRange () {
		if (_dataSheet.Surrend)
			return new List<Unit>(); //You cannot be helped if you surrend
		List<Unit> tmp = new List<Unit>();
		foreach (Unit u in _unitBoardList) {
			int i = u.Helps(this);
			if (i > 0) {
				tmp.Add(u);
				Tile.HighlightTile(u.GetCurrentTile, Tile.EEffectType.Shield);
				u.GetTextRenderer.Print(""+i, TextRenderer.ETextRenderType.Support);
			}
		}
		return tmp;
	}

	///<summary>
	/// Highlights and returns a list of unit that can hit this unit
	///</summary>
	public List<Unit> AttackedByRange () {
		List<Unit> tmp = new List<Unit>();
		foreach (Unit u in _unitBoardList) {
			int i = u.Hits(this);
			if (i > 0) {
				tmp.Add(u);
				Tile.HighlightTile(u.GetCurrentTile, Tile.EEffectType.Range);
				u.GetTextRenderer.Print(""+i, TextRenderer.ETextRenderType.Danger);
			}
		}
		return tmp;
	}
	

	///<summary>
	/// Tries to moves to a tile, if the tile isnt in the range of the unit, return false.
	/// Calling <see cref="MovingRange()"/> is needed to compute the range of the unit before trying to move it.
	/// </summary>
	public bool Move (Tile destination) {
		if (_dataSheet.Surrend)
			return false;
		if (_movedThisTurn)
			return false;

		bool moved = false;
		if (_range.Contains(destination)) {
			//If we move, we unbake every action of the unit, and we mark the unit as unbaked in every action that can target it
			foreach (Attack a in _attackList)
				a.Unbake();
			foreach (Action a in _targetedByActionList)
				if (a is Attack) {
					Attack at = (Attack)a;
					at.Unbake(this);
				}

			//If we enter/leave/move in an action zone that we can block, we also unbake the action
			foreach (Action a in _blocksActionList) {
				if (a.GetRangeShape.EnterLeaveMove(a, _currentTile, destination))
					if (a is Attack) {
						Attack at = (Attack)a;
						at.Unbake(this);
					}
			}

			SetCurrentTile(destination);
			moved = true;
		}
		_range.Clear();
		Tile.RemoveAllHighlightedTiles();

		_movedThisTurn = moved;

		return moved;
	}

	///Unbakes all actions related to this unit
	public void Unbake() {
		foreach (Attack a in _attackList)
			a.Unbake();
		foreach (Action a in _targetedByActionList)
			if (a is Attack) {
				Attack at = (Attack)a;
				at.Unbake(this);
			}

		foreach (Action a in _blocksActionList) {
			if (a is Attack) {
				Attack at = (Attack)a;
				at.Unbake(this);
			}
		}
	}

	public override String ToString() {
		String ans = "===== Unit =====\n";
			ans += base.ToString() + '\n';
			ans += "Blocked by :";
			foreach (String s in _blockedBy) {
				ans += " " + s;
			}
			ans += '\n';
			ans += "Defense : " + _defense + '\n';
			ans += "Movement : " + _movement.ToString() + '\n';
			if (_currentTile != null) { //Position
				ans += "       X : " + _currentTile.X + '\n';
				ans += "       Y : " + _currentTile.Y + '\n';
			}
			ans += "===== ATTACKS =====" + '\n'; //Attacks
			for (int i = 0; i < _attackList.Count; i++) {
				ans += _attackList[i].ToString() + '\n';
			}
			ans += "===== PASSIVES ====\n"; //Passives
			for (int i = 0; i < _passiveList.Count; i++) {
				ans += _passiveList[i].ToString() + '\n';
			}
		return ans + '\n';
	}

	/// <summary>
	/// Kills this unit, taking care of removing this unit from every action that can target it, and removing its action from each unit that can be targeted by it
	/// </summary>
	public void Kill () {
		//Removing the units from other unit's actions
		foreach (Action a in _targetedByActionList) {
			a.RemoveUnitFromTargets(this);
		}
		foreach (Action a in _blocksActionList) {
			if (a is Attack) {
				Attack at = (Attack)a;
				at.RemoveBlockingUnit(this);
			}
		}

		//Removing the unit's actions from other units
		foreach (Attack a in _attackList) {
			a.RemoveAction();
		}
		foreach (Passive p in _passiveList) {
			p.RemoveAction();
			if (p is Radio) {
				Radio r = (Radio)p;
				r.SignalLine.QueueFree();
				r.SignalLine = null;
			}
		}

		//Removing the unit for players
		foreach (Player p in Player.GetPlayerList) {
			p.OnUnitKilled(this);
		}

		//Removing the unit from the board
		_unitBoardList.Remove(this);
		_currentTile.SetLocalUnit(null);
		_currentTile = null;
		TextRenderer.TextRendererList.Remove(_textRenderer);
		_textRenderer.QueueFree();
		QueueFree();
	}

	public void Flee () {

	}

	public static Unit SearchUnit (String unitTitle) {
		foreach (Unit unit in _unitList) {
			if (unit.GetTitle.Equals(unitTitle, StringComparison.OrdinalIgnoreCase)) {
				return unit;
			}
		}

		return null;
	}

	///<summary>
	/// should be called when all units are present on the board, it fills the actions '_targetableUnitList' arrays
	///</summary>
	public void InitializeActions () {
		foreach (Attack a in _attackList) {
			a.InitialiseTargetableUnits();
		}
		foreach (Passive p in _passiveList) {
			p.InitialiseTargetableUnits();
		}
	}
}
