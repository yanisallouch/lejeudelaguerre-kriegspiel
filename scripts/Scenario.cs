using Godot;
using System;
using System.Collections.Generic;


/// <summary>
/// A scenario stores Player data and makes the game go forward. If someone wins/looses, the scenario detects it and set it as a winner/looser (TODO)
/// </summary>
/// <param name="playerList"> a list that stores the players </param>
/// <param name="currentPlayer">  the current player number  </param>
/// <param name="leftText"> display that gives directions to players </param>    
public class Scenario : Node
{
    private List<Player> _playerList = new List<Player>();
    private int _currentPlayer = 0;
    private RichTextLabel _leftText; public RichTextLabel SetLeftText { set => _leftText = value; }
    private bool _initialized = false;

    public Scenario (List<Player> playerList) {
        SetName("SCENARIO");
        _playerList = playerList;
        System.Console.WriteLine("PlayerListCount: " + playerList.Count);
        System.Console.WriteLine("Player 0 UnitCount: " + playerList[0].GetUnitList.Count);
        System.Console.WriteLine("Player 1 UnitCount: " + playerList[1].GetUnitList.Count);
        foreach(Player p in playerList) {
            AddChild(p);
        }
    }

    /// <summary>
    /// Steps the game forward, phase by phase, player by player
    /// </summary>
    public void Step () {
        if (_playerList[_currentPlayer].NextState()) { //Steps the current player forward, if nextState returns true, the player finished his turn
            //Here we do things once the board is initialized (all units have been placed by all players)
            if (!_initialized) {
                bool initialize = true;

                foreach (Player p in _playerList) {
                    if (!p.Initialized)
                        initialize = false;
                }

                if (initialize) {
                    _initialized = true;
                    foreach (Player p in _playerList) {
                        foreach (Unit u in p.GetUnitList) {
                            u.InitializeActions();
                        }
                    }
                }
            }

            //Here we change player
            _currentPlayer++;
            if (_currentPlayer >= _playerList.Count) {
                _currentPlayer = 0;
            }
            _playerList[_currentPlayer].NewTurn();
        }

        _leftText.Text = _playerList[_currentPlayer].InfoStringLeft();

    }

    public void Random () {
        if (_playerList[_currentPlayer].NextState()) { //Steps the current player forward, if nextState returns true, the player finished his turn
            _currentPlayer++;
            if (_currentPlayer >= _playerList.Count) {
                _currentPlayer = 0;
            }
            _playerList[_currentPlayer].NewTurn();
        }

        _leftText.Text = _playerList[_currentPlayer].InfoStringLeft();
    }
}