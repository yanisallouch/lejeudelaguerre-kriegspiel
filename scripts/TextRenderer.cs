using Godot;
using System;
using System.Collections.Generic;

/// <summary>
/// describes a Text renderer, it will be used to display informations above units
/// </summary>
public abstract class TextRenderer : Node2D {
	public enum ETextRenderType {
		Info,
		Danger,
		Support
	};

	public interface IRendererData {}

	private Spatial _followedNode = null;
	private static Camera _camera = null;
	private static List<TextRenderer> _textRendererList = new List<TextRenderer>();
	public static List<TextRenderer> TextRendererList {get => _textRendererList; }

	public TextRenderer (TextRenderer textRenderer, Spatial followedNode, Node rootNode) {
		_followedNode = followedNode;
		Root.RootNode.GetNode("./2D/UnitTextRenderers").AddChild(this);
		_textRendererList.Add(this);
	}

	public TextRenderer (Node rootNode) {
		if (_camera == null) {
			_camera = (Camera)Root.RootNode.GetNode("./CameraGroup/Camera");
		}
	}

	public override void _Process (float delta) {
		if (_followedNode.IsInsideTree() && Visible)
			Position = _camera.UnprojectPosition(_followedNode.GlobalTransform.origin);
	}

	public abstract bool Config (Godot.Collections.Dictionary textRendererDict);
	public abstract void Show (bool b);
	public abstract void Print (String text, ETextRenderType type);
	public abstract void Clear ();
	public static void ClearAll () {
		foreach (TextRenderer tr in _textRendererList) {
			tr.Clear();
		}
	}

}




