using Godot;
using System;

/// <summary>
/// describes a Renderer, it will be used to print units and terrain to the screen
/// </summary>
public abstract class Renderer : Spatial {
    public interface IRendererData {}

    public Renderer (Renderer renderer) {}

    public Renderer () {}

    public abstract void AddAnimation(IRendererData data, String animationName);
    public abstract void PlayAnimation (String animationName);
    public abstract bool Config (Godot.Collections.Dictionary rendererDict, String path, String animationName);
    public abstract void Show (bool b);
    public abstract void Skin (int team);
    public abstract Renderer Clone (String defaultAnimation);
    public static Renderer StrToRenderer (String renderer) {
        switch (renderer) {
            case "SpriteRenderer":
                return new SpriteRenderer();
            default:
                return null;
        }
    }
}