using Godot;
using System;
using System.Collections.Generic;

public class Radio : Passive {
    public enum ERadioType {
        RadioStation, //Source of the radio signal
        Relay, //Relays a radio signal
        Reciever //Can act only if recieving a radio signal
    }

    public bool Connected {get; set;} = false;
    private ERadioType _type = ERadioType.RadioStation; public ERadioType Type {get => _type;}
    public List<Radio> TargetableRadioList {get;} = new List<Radio>(); //The list of radio passives that this radio can interact with
    public Radio ParentSignal {get; set;} = null;
    public List<Radio> ChildSignals {get;} = new List<Radio>();
    public Line SignalLine = null;


    public Radio (Radio radio, Unit parentUnit) : base(radio, parentUnit) {
        _type = radio._type;
    }

    public Radio (String title, String description, List<String> filter, Shape shape, ETarget filterIgnore, List<String> blockedBy, ETarget blockedByIgnore)
    : base (title, description, filter, shape, filterIgnore, blockedBy, blockedByIgnore) {

    }

    public override void Init () {
        Connected = false;
        ParentSignal = null;
        ChildSignals.Clear();

        if (SignalLine == null) {
            SignalLine = new SimpleLine();
            GetParentUnit.AddChild(SignalLine);
        }
        SignalLine.Start = null;
        SignalLine.End = null;
        
        System.Console.WriteLine("inited" + this);
    }

    //Should be called first on RadioStation, then Relay, then Reciever
    public override void Do () {
        System.Console.WriteLine("do" + _type);
        if (_type == ERadioType.Reciever) {
            System.Console.Write("reciever :");
            if (Connected) {
                System.Console.WriteLine("connected");
                GetParentUnit.GetDataSheet.Surrend = false;
            } else {
                System.Console.WriteLine("not connected");
                GetParentUnit.GetDataSheet.Surrend = true;
            }
            return;
        }
        if (_type == ERadioType.Relay) { //A relay doesn't need to do anything
            return;
        }
        //We are a radiostation, we do a BFS
        Queue<Radio> queue = new Queue<Radio>();
        queue.Enqueue(this);
        Radio current;
        while (queue.Count > 0) {
            current = queue.Dequeue();
            foreach(Radio r in current.TargetableRadioList) {
                if (!r.Connected && current.GetRangeShape.SelectPointOfViewOf(current, r.GetParentUnit)) {
                    //r isnt already connected, & current can reach r
                    if (r.Type == ERadioType.Relay)
                        queue.Enqueue(r); //The signal will be relayed
                    if (r.Type == ERadioType.Relay || r.Type == ERadioType.Reciever) {
                        current.ChildSignals.Add(r);
                        r.ParentSignal = current;
                        r.Connected = true;
                        r.SignalLine.Start = current.GetParentUnit.GetCurrentTile;
                        r.SignalLine.End = r.GetParentUnit.GetCurrentTile;
                        r.SignalLine.Draw = true;
                    }
                }
            }
        }
    }

    public override void RemoveUnitFromTargets(Unit u) {
        base.RemoveUnitFromTargets(u);
        foreach (Passive p in u.GetPassiveList) {
            if (p is Radio) {
                TargetableRadioList.Remove((Radio)p);
            }
        }
    }

    public override void InitialiseTargetableUnits () {
        base.InitialiseTargetableUnits();
        //we retrieve the radio passives from the units that can be targeted by this passive
        foreach (Unit u in GetTargetableUnitList) {
            foreach (Passive p in u.GetPassiveList) {
                if (p is Radio) {
                    TargetableRadioList.Add((Radio)p);
                }
            }
        }
    }

    public override bool Config (Godot.Collections.Dictionary data) {
        if (!data.Contains("type") || !(data["type"] is String)) {
			System.Console.WriteLine("Error : passive");
			return false;
		}
		_type = StrToERadioType((String)data["type"]);
        if ((int)_type == -1) {
            System.Console.WriteLine("Error : passive.value");
			return false;
        }

        //We change the execution priority
        switch (_type) {
            case ERadioType.RadioStation:
                Priority = 10;
                break;
            case ERadioType.Relay:
                Priority = 9;
                break;
            case ERadioType.Reciever:
                Priority = 8;
                break;
        }

        return true;
    }

    private ERadioType StrToERadioType (String str) {
        switch (str) {
            case "RadioStation":
                return ERadioType.RadioStation;
            case "Relay":
                return ERadioType.Relay;
            case "Reciever":
                return ERadioType.Reciever;
        }
        return (ERadioType)(-1);
    }

    public override Passive Clone (Unit parentUnit) {
        return new Radio(this, parentUnit);
    }   
}