using Godot;
using System;
using System.Collections.Generic;

public abstract class Action : Entity {
    private Shape _rangeShape; public Shape GetRangeShape{ get => _rangeShape; }
    private Unit _parentUnit; public Unit GetParentUnit{ get => _parentUnit; }

    //Targeting stuff
    private List<String> _filter = new List<String>(); public List<String> GetFilterList { get => _filter; }
    private ETarget _filterIgnore; public ETarget _GetTarget { get => _filterIgnore; }

    //Blocking stuff
    private List<String> _blockedBy = new List<String>();
    private ETarget _blockedByIgnore;

    //Optimisation
    private List<Unit> _targetableUnitList = new List<Unit>(); public List<Unit> GetTargetableUnitList { get => _targetableUnitList; }
 

    /// <summary>
    /// Returns the list of unit that can be hit by this action
    /// </summary>
    public abstract List<Unit> SelectRange ();

    /// <summary>
    /// removes a unit from the target lists. Should be called when u is killed
    /// </summary>
    public virtual void RemoveUnitFromTargets (Unit u) {
        _targetableUnitList.Remove(u);
    }

    public void RemoveAction () {
        foreach(Unit u in _targetableUnitList) {
            u.TargetedByActionList.Remove(this);
        }
    }

    public enum ETarget {
        All,
        Allies,
        Enemies,
        Noone
    }


    /// <summary>
    /// Instantiates an action as a child of a unit
    /// </summary>
    public Action (Action action, Unit parentUnit)
    : base (action) {
        _rangeShape = new Shape(action._rangeShape);
        _parentUnit = parentUnit;

        _filter = new List<String>(action._filter);
        _filterIgnore = action._filterIgnore;

        _blockedBy = new List<String>(action._blockedBy);
        _blockedByIgnore = action._blockedByIgnore;

    }

    public Action (String title, String description, List<String> filter, Shape shape, ETarget filterIgnore, List<String> blockedBy, ETarget blockedByIgnore, Unit parentUnit)
    : base(title, description) {
        _rangeShape = shape;
        _parentUnit = parentUnit;

        _filter = filter;
        _filterIgnore = filterIgnore;
        
        _blockedBy = blockedBy;
        _blockedByIgnore = blockedByIgnore;
    }

    /// <summary>
    /// Initializes all of the arrays used to target units, should be called only once, at the beggining of the game, when all of the units are present on the board
    /// </summary>
    public virtual void InitialiseTargetableUnits () {
        _targetableUnitList.Clear();
        foreach (Unit u in Unit.GetUnitBoardList) {
            if (canTarget(u)) {
                _targetableUnitList.Add(u);
                u.TargetedByActionList.Add(this);
            }
            if (BlockedBy(u)) {
                u.BlocksActionList.Add(this);
            }
        }

        _parentUnit.Unbake();
    }

    /// <summary>
    /// Returns true if the action is blocked by the entityPhysical
    /// </summary>
    private bool BlockedBy (EntityPhysical entity) {
        if (entity == null)
            return false;
            
        bool ignore = false;

        switch (_blockedByIgnore) {
            case ETarget.All:
                ignore = true;
                break;
            case ETarget.Allies:
                ignore = _parentUnit.GetTeam == entity.GetTeam;
                break;
            case ETarget.Enemies:
                ignore = _parentUnit.GetTeam != entity.GetTeam;
                break;
            case ETarget.Noone:
                ignore = false;
                break;
        }
        
        if (ignore)
            return false;

        foreach(String item in _blockedBy) {
            foreach (String tag in entity.GetTagList) {
                if (item.Equals(tag, StringComparison.OrdinalIgnoreCase)) {
                    return true;
                }
            }
        }

        return false;
    }

    /// <summary>
    /// Returns true if the action is blocked by the tile (either by its terrain or unit)
    /// </summary>
    public bool IsBlocked (Tile tile) {
        if (tile.GetLocalTerrain != null) {
            if (BlockedBy(tile.GetLocalTerrain))
                return true;
        }
        if (tile.GetLocalUnit != null) {
            if (BlockedBy(tile.GetLocalUnit))
                return true;
        }
        return false;
    }

    ///<summary>
    /// returns true if the entityPhysical e can be targeted by the action, based on its filters and target value (teams)
    ///</summary>
    private bool canTarget (EntityPhysical e) {
        bool tagged = false;
        bool targeted = false;
        
        foreach (String filter in _filter) {
            foreach (String tag in e.GetTagList) {
                if (filter.Equals(tag, StringComparison.OrdinalIgnoreCase)) {
                    tagged = true;
                    break;
                }
            }
        }

        switch (_filterIgnore) {
            case ETarget.Noone:
                targeted = true;
                break;
            case ETarget.Allies:
                if (e.GetTeam != _parentUnit.GetTeam)
                    targeted = true;
                break;
            case ETarget.Enemies:
                if (e.GetTeam == _parentUnit.GetTeam)
                    targeted = true;
                break;
        }

        return tagged && targeted;
    }

    public static ETarget StrToETarget (String str) {
        switch (str) {
			case "All":
				return Action.ETarget.All;
			case "Allies":
				return Action.ETarget.Allies;
			case "Enemies":
				return Action.ETarget.Enemies;
			case "Noone":
			    return Action.ETarget.Noone;
			default:
				return (ETarget)(-1);
		}
    }
}