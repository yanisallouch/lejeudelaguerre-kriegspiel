using Godot;
using System;
using System.Collections.Generic;

/// <summary>
/// describes a shape, and can tell if a coodinate is in that shape. 
/// <code>
/// x           x           x   vertical    true
///   x         x         x     diagonal    true
///     x       x       x       range       4
///       x x x x x x x         mask        2
///       x x x x x x x         maxRange    7
///       x x       x x  
/// x x x x x   o   x x x x x
///       x x       x x  
///       x x x x x x x  
///       x x x x x x x  
///     x       x       x
///   x         x         x
/// x           x           x
/// </code>
/// </summary>
public class Shape : Node {
    private bool _vertical;
    private bool _diagonal;
    private int _range;
    private int _maxRange;
    private int _mask;

    public Shape (Shape shape) {
        _vertical = shape._vertical;
        _diagonal = shape._diagonal;
        _range = shape._range;
        _maxRange = shape._maxRange;
        _mask = shape._mask;
    }

    /// <summary>
    /// Instantiates a Shape
    /// </summary>
    /// <param name="vertical"> select all the squares in the horizontal and vertical plane </param>
    /// <param name="diagonal"> select all the squares in the diagonal plane </param>
    /// <param name="range"> select all the squares at n squares of the origin </param>
    /// <param name="vertical"> unselect all the squares at n squares of the origin </param>
    public Shape (bool vertical, bool diagonal, int range, int mask, int maxRange) {
        _vertical = vertical;
        _diagonal = diagonal;
        _range = range;
        _maxRange = maxRange;
        _mask = mask;
    }

    public List<Tile> TileRange (Unit caster) {
        List<Tile> ans = new List<Tile>();
        for (int x = -_range + 1; x < _range; x++) {
            for (int y = -_range + 1; y < _range; y++) {
                if (Mathf.Abs(x) >= _mask && Mathf.Abs(y) >= _mask) {
                    Tile tmp = caster.GetCurrentTile.getTile(caster.GetCurrentTile.X + x, caster.GetCurrentTile.Y + y);
                    if (tmp != null) {
                        ans.Add(tmp);
                    }
                }
            }
        }
        return ans;
    }

    /// <summary>
    /// Returns true if the movement described by src and dst is entering, leaving moving inside the shape, (returns false only if the source and the destination are outside of the shape)
    /// </summary>
    public bool EnterLeaveMove (Action a, Tile src, Tile dst) {
        bool srcIn = inShape(a.GetParentUnit.GetCurrentTile, src);
        bool dstIn = inShape(a.GetParentUnit.GetCurrentTile, dst);
        return !(srcIn == false && dstIn == false);
    }

    /// <summary>
    /// Uses raycasts to check if the parent unit can shoot the target. Returns true if it can
    /// </summary>
    public bool SelectPointOfViewOf (Action action, Unit target) {
        Unit watchingUnit = action.GetParentUnit;

        if (target != null) {
            //We can action the unit u, here we will check if the unit is in range and visible
            if (inShape(watchingUnit.GetCurrentTile, target.GetCurrentTile)) {
                Vector3 from = watchingUnit.GetCurrentTile.Translation + (Vector3.Up * 0.1f);
                Vector3 to = target.GetCurrentTile.Translation + (Vector3.Up * 0.1f);

                PhysicsDirectSpaceState spaceState = watchingUnit.GetWorld().DirectSpaceState;
                Godot.Collections.Dictionary result;
                Godot.Collections.Array resultArray = new Godot.Collections.Array();
                resultArray.Add(null);

                //Here we raycast, hitting colliders untill we reach the target, 
                //or until we reach a collider that is attached to a tile that blocks the action
                while (true) {
                    result = spaceState.IntersectRay(from, to, resultArray, (int)Tile.ERaycastLayer.POVRaycasting);

                    if (result.Count == 0)
                        return true;

                    Node n = (Node)result["collider"];
                    Tile t = (Tile)n.GetParent();

                    if (t.GetLocalUnit != null && t.GetLocalUnit == target) {
                        return true;
                    }

                    if (action.IsBlocked(t))
                        return false;

                    from = (Vector3)result["position"];
                    resultArray[0] = n;
                }
            }
        }
        return false;
    }

    /// <summary>
    /// returns true if the tile dest is in the shape relative to the tile source
    /// </summary>
    public bool inShape (Tile source, Tile dest) {
        if (Math.Abs(source.X - dest.X) <= _mask && Math.Abs(source.Y - dest.Y) <= _mask) //Minimum range mask
            return false;
        if (Math.Abs(source.X - dest.X) > _maxRange && Math.Abs(source.Y - dest.Y) > _maxRange) //Max range mask
            return false;
        if (_vertical && (source.X == dest.X || source.Y == dest.Y)) //Vertical
            return true;
        if (_diagonal && Math.Abs(source.X - dest.X) == Math.Abs(source.Y - dest.Y)) //Diagonal
            return true;
        if (Math.Abs(source.X - dest.X) < _range && Math.Abs(source.Y - dest.Y) < _range) //Range
            return true;
        return false;
    }

    /// <summary>
    /// Returns a list of tile that the unit 'movingUnit' can walk on
    /// </summary>
    public List<Tile> SelectMovementOf(Unit movingUnit, int buff) {
        Queue<Tile> queue = new Queue<Tile>();
        Queue<Tile> parent = new Queue<Tile>();
        Queue<int> level = new Queue<int>();
        List<Tile> discovered = new List<Tile>();
        List<Tile> walkable = new List<Tile>();
        Tile currentTile;
        int currentLevel;
        int x;
        int y;
        int unitX = movingUnit.GetCurrentTile.X;
        int unitY = movingUnit.GetCurrentTile.Y;
        queue.Enqueue(movingUnit.GetCurrentTile);
        level.Enqueue(1);
        discovered.Add(movingUnit.GetCurrentTile);

        //Breadth First Search
        while (queue.Count != 0) {
            currentTile = queue.Dequeue();
            currentLevel = level.Dequeue();

            x = currentTile.X;
            y = currentTile.Y;

            //We first check if the next 'vertex' is a valid one
            if (currentLevel < _maxRange + buff) { //The next tile will be inside of the max range radius 
                for (int i = -1; i <= 1; i++) { //x +- 1
                    for (int j = -1; j <= 1; j++) { //y +- 1
                        // x x x
                        // x . x
                        // x x x
                        if (!(i == 0 && j == 0)) { //not the Current tile
                            bool exec = false;
                            // x . x
                            // . o .
                            // x . x
                            if (_diagonal && Math.Abs((x + i) - unitX) == Math.Abs((y + j) - unitY)) { //We are on the diagonals of the unit
                                exec = true;
                            }
                            // . x .
                            // x o x
                            // . x .
                            if (_vertical && ((x + i) == unitX || (y + j) == unitY)) { //We are on the verticals of the unit
                                exec = true;
                            }

                            if (currentLevel < _range + buff) { //We are inside of the range diameter
                                exec = true;
                            }

                            if (exec) { //We execute if we are on a tile that can be selected by this shape
                                Tile tmpTile = currentTile.getTile(x + i, y + j);
                                if (tmpTile != null) { //returned a tile
                                    //The unit can walk on this 'vertex', we do the BFS stuff
                                    if (!discovered.Contains(tmpTile)) { //We havent already discovered this tile
                                        discovered.Add(tmpTile);
                                        Terrain tmpTerrain = tmpTile.GetLocalTerrain;
                                        Unit tmpUnit = tmpTile.GetLocalUnit;

                                        bool terrainBlock = movingUnit.BlockedBy(tmpTerrain);
                                        bool unitBlock = movingUnit.BlockedBy(tmpUnit);

                                        if (!terrainBlock && !unitBlock) { //neither a unit or a terrain blocks the movement of the caster
                                            //Those 'vertexes' are part of the graph, we queue them
                                            queue.Enqueue(tmpTile);
                                            level.Enqueue(currentLevel + 1);
                                            if (tmpUnit == null && currentLevel >= _mask) { //If there is no unit on the selected tile, and we are not in the mask
                                                //The vertex is walkable
                                                walkable.Add(tmpTile);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return walkable;
    }


    public override String ToString () {
        String ans = "Shape : vertical/horizontal : " + _vertical + '\n';
              ans += "        diagonals : " + _diagonal + '\n';
              ans += "        range : " + _range + '\n';
              ans += "        mask : " + _mask;
              ans += "        max range : " + _maxRange;
        return ans;
    }
}
