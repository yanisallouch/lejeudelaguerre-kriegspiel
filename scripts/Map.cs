using Godot;
using System;
using System.Collections.Generic;

/// <summary>
/// Map représente le plateau de jeu qui est instanciée par <see cref="Entity"/> and <see cref="EntityBuilder">, TODO
/// </summary>
// /// <param name="localEntiy"> l'entité qui sert à afficher le nom et la description de la map </param>
/// <param name="length"> longeur de la map </param>
/// <param name="width"> largeur de la map </param>    

public class Map : Entity {

    //private EntityBuilder _localEntity; public EntityBuilder GetlocalEntity { get => _localEntity; }
	private Tile[,] _map;
	private int _length;
	private int _width;
	private float _offset = 1f; public float GetOffset { get => _offset; }
	private List<Unit> _listUnits;
	private Tile _defaultTile;
	private Scenario _scenario; public Scenario GetScenario { get => _scenario; }
	private CanvasGridDrawer _canvasDrawer;

	public int GetLength() {
		return _length;
	}

	public void SetLength(int length) {
		_length = length;
	}

	public int GetWidth() {
		return	_width;
	}

	public void SetWidth(int width){
		_width = width;
	}

	public List<Unit> GetListUnits() {
		return _listUnits;
	}

	public void SetListUnits(List<Unit> listUnits) {
		_listUnits = listUnits;
	}

	public Map (String title, String description, int width, int length, Tile defaultTile, Scenario scenario) : base(title, description) {
		_width = width;
		_length = length;
		_defaultTile = defaultTile;
		_scenario = scenario;
		_canvasDrawer = new CanvasGridDrawer(width, length);
		AddChild(_canvasDrawer);
		AddChild(scenario);

		initTile();
	}

	public void initTile(){		
		_map = new Tile[GetLength(), GetWidth()]; 
		for(int i = 0; i < GetLength(); i++) {
			for(int j = 0; j < GetWidth(); j++) {
				Tile tmp = new Tile(_defaultTile, i,j);
				if (i % 3 == 0 && j > 7 && j < 12 )
					tmp.SetLocalTerrain(new Terrain(Terrain.GetTerrain("Montagne")));
				else
					tmp.SetLocalTerrain(new Terrain(Terrain.GetTerrain("Plaine")));
				tmp.SetMap = this;
				AddChild(tmp);
				_map[i, j] = tmp;
				float xTranslation = -i * _offset;
				float yTranslation = -j * _offset;
				_map[i, j].Translate(new Vector3(xTranslation, 0, yTranslation));
			}
		}
	}

	public Tile[,] GetMap() {
		return _map;
	}

	public void SetMap(Tile[,] value) {
		_map = value;
	}

	public Tile getTile (int x, int y) {
		if (x < 0 || x >= _length || y < 0 || y >= _width)
			return null;
        return _map[x, y];
    }

	public override String ToString() {
		return "Longeur : " + _length + " Largeur : " + _width + '\n';
	}
}