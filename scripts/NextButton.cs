using Godot;
using System;

public class NextButton : Button
{
	private Scenario _scenario; 
	public Scenario SetScenario { set => _scenario = value; }
	
	public override void _Ready() {
	}

	public override void _Pressed() {
		_scenario.Step();
	}
}
