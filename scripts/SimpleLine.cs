using Godot;
using System;

public class SimpleLine : Line {
	private Camera _cam;
	private Color _col = new Color(0.2f, 1f, 0.5f);
	public SimpleLine () : base () {

	}

	public SimpleLine (Color col) : base () {
		_col = col;
	}

	public override void _Ready() {
		_cam = (Camera)GetTree().Root.GetNode("/root/WorldEnvironment/CameraGroup/Camera");
	}

	public override void _Process (float delta) {
		if (Start != null && End != null) {
			Update();
		}
	}

	public override void _Draw () {
		if (Start != null && End != null) {
			Vector2 A = _cam.UnprojectPosition(Start.Translation);
			Vector2 B = _cam.UnprojectPosition(End.Translation);
			DrawLine(A, B, _col, 2, true);
		}
	}
}
