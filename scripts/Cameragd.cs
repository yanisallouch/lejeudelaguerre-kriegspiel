using Godot;
using System;

/// <summary>
/// A simple camera script that should be attached to the main camera
/// </summary>
public class Cameragd : Camera {
    private Vector3 _momentum = new Vector3(0f, 0f, 0f);
    private Vector3 _targetMovement = new Vector3(0f, 0f, 0f);
    private float _speedMaxZoom = 0.0025f;
    private float _speedMinZoom = 0.025f;
    private float _speed;
    private float _smoothing = 0.2f;
    private int _side = 70;
    private float _zoom = 1f;
    private float _zoomSpeed = 0.5f;
    private float _currentZoom = 1f;
    private float _minZoom = 1f;
    private float _maxZoom = 20f;
    private Vector2 _viewportSize;
	private bool _keyboardAuthority = false;

    //Temporary, should be moved to a "Player" class
    Unit selectedUnit = null;

    public override void _Ready() {
        _speed = Mathf.Lerp(_speedMaxZoom, _speedMinZoom, _zoom);
        _viewportSize = GetViewport().GetSize();
    }

    /// <summary>
    /// We update the momentum of the camera based on the targeted movement (</see href="_Input(<InputEvent>)">), and the move it accordingly
    /// </summary>
    public override void _Process(float delta)
    {
        //The momentum is used to give the camera a smoother movement
        _momentum.x = Mathf.Lerp(_momentum.x, _targetMovement.x, _smoothing);
        _momentum.y = Mathf.Lerp(_momentum.y, _targetMovement.y, _smoothing);

        _currentZoom = Mathf.Lerp(_currentZoom, _zoom, _smoothing);

        this.Translate(_momentum * _speed);
        this.Set("size", _currentZoom);
    }

    /// <summary>
    /// When the user inputs something (like a mouse movement or a keypress), we process the targeted movement of the camera
    /// </summary>
    public override void _Input(InputEvent @event) {
        //Zoom is related to the mouse wheel
        if (@event is InputEventMouseButton) {
            InputEventMouseButton emb =  (InputEventMouseButton)@event;
            if (emb.IsPressed()){
                if (emb.ButtonIndex == (int)ButtonList.WheelUp){
                    _zoom -= _zoomSpeed;
                }
                if (emb.ButtonIndex == (int)ButtonList.WheelDown){
                    _zoom += _zoomSpeed;
                }
                _zoom = Mathf.Clamp(_zoom, _minZoom, _maxZoom);
            }
            _speed = Mathf.Lerp(_speedMaxZoom, _speedMinZoom, _zoom);
        }
        //Here we move the camera based on the mouse position, if no directional keys are pressed
        if (@event is InputEventMouseMotion && !_keyboardAuthority) {
            InputEventMouseMotion emm = (InputEventMouseMotion)@event;
            if (emm.Position.x < _side) {
                _targetMovement.x = -1;
            } else if (emm.Position.x > _viewportSize.x - _side) {
                _targetMovement.x = 1;
            } else {
                _targetMovement.x = 0;
            }
            if (emm.Position.y < _side) {
                _targetMovement.y = 1;
            } else if (emm.Position.y > _viewportSize.y - _side) {
                _targetMovement.y = -1;
            } else {
                _targetMovement.y = 0;
            }
        }
        //Here we move the camera with directional keys
        if (@event is InputEventKey) {
            _keyboardAuthority = false;
            _targetMovement.x = 0;
            _targetMovement.y = 0;
            if (Input.IsKeyPressed((int)KeyList.Right)) {
                _targetMovement.x += 1;
                _keyboardAuthority = true;
            }
            if (Input.IsKeyPressed((int)KeyList.Left)) {
                _targetMovement.x -= 1;
                _keyboardAuthority = true;
            }
            if (Input.IsKeyPressed((int)KeyList.Up)) {
                _targetMovement.y += 1;
                _keyboardAuthority = true;
            }
            if (Input.IsKeyPressed((int)KeyList.Down)) {
                _targetMovement.y -= 1;
                _keyboardAuthority = true;
            }
        }

        //Here we raycast on click
        //Temporary, should be moved to a "Player" class
        if (@event is InputEventMouseButton) {
            InputEventMouseButton emb = (InputEventMouseButton)@event;
            if (emb.ButtonIndex == (int)ButtonList.Left) {
                Camera camera = this;
                Vector3 from = camera.ProjectRayOrigin(emb.Position);
                Vector3 to = from + camera.ProjectRayNormal(emb.Position) * 1000;

                PhysicsDirectSpaceState spaceState = GetWorld().DirectSpaceState;
                Godot.Collections.Dictionary result = spaceState.IntersectRay(from, to);

                if (result.Count > 0) {
                    Node n = (Node)result["collider"];
                    Tile t = (Tile)n.GetParent();
                    if (t.GetLocalUnit != null) {
                        t.GetLocalUnit.MovingRange();
                        selectedUnit = t.GetLocalUnit;
                    } else {
                        if (selectedUnit != null)
                            selectedUnit.Move(t);
                    }
                }
            }
        }
    }
}
