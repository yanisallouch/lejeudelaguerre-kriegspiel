using Godot;
using System;

/// <summary>
/// describes a Text renderer, it will be used to display informations above units
/// </summary>
public class RichTextRenderer : TextRenderer {
	private RichTextLabel _richTextLabel;
	private String _text = "";

	private string _infoCC = "#FFFFFF";
	private string _dangerCC = "#FF0000";
	private string _supportCC = "#00FF00";

	public RichTextRenderer (RichTextRenderer richTextRenderer, Spatial followedNode, Node rootNode) : base(richTextRenderer, followedNode, rootNode) {
		_infoCC = richTextRenderer._infoCC;
		_dangerCC = richTextRenderer._dangerCC;
		_supportCC = richTextRenderer._supportCC;
		_richTextLabel = new RichTextLabel();
		_richTextLabel.Text = "";
		_richTextLabel.MarginBottom = 50;
		_richTextLabel.MarginTop = -50;
		_richTextLabel.MarginRight = 50;
		_richTextLabel.MarginLeft = -50;
		_richTextLabel.BbcodeEnabled = true;
		//_richTextLabel.RectScale = new Vector2 (2, 2);
		
		AddChild(_richTextLabel);
	}

	public RichTextRenderer (Node sceneNode) : base(sceneNode) {
		_richTextLabel = new RichTextLabel();
		_richTextLabel.Text = "";
		AddChild(_richTextLabel);
	}

	public override bool Config (Godot.Collections.Dictionary textRendererDict) {
		//Info
		if (!textRendererDict.Contains("info") || !(textRendererDict["info"] is String)) {
			System.Console.WriteLine("Error : textrenderer.info");
			return false;
		}
		_infoCC = (String)textRendererDict["info"];

		//Danger
		if (!textRendererDict.Contains("danger") || !(textRendererDict["danger"] is String)) {
			System.Console.WriteLine("Error : textrenderer.danger");
			return false;
		}
		_dangerCC = (String)textRendererDict["danger"];

		//Help
		if (!textRendererDict.Contains("info") || !(textRendererDict["support"] is String)) {
			System.Console.WriteLine("Error : textrenderer.support");
			return false;
		}
		_supportCC = (String)textRendererDict["support"];

		return true;		
	}

	public override void Show (bool b) {
		if (b)
			Show();
		else
			Hide();
	}

	public override void Print (String text, ETextRenderType type) {
		string tagA = "";
		string tagB = "";
		switch (type) {
			case ETextRenderType.Info:
				tagA = "[color=" + _infoCC + "]";
				tagB = "[/color]";
				break;
			case ETextRenderType.Danger:
				tagA = "[color=" + _dangerCC + "]";
				tagB = "[/color]";
				break;
			case ETextRenderType.Support:
				tagA = "[color=" + _supportCC + "]";
				tagB = "[/color]";
				break;
		}
		_text += '\n' + tagA + text + tagB;
		_richTextLabel.BbcodeText = "[center][font=./Build/Addons/Kriegsspiel/Fonts/NK57.tres]" + _text + "[/font][/center]";
	}

	public override void Clear () {
		_richTextLabel.Clear();
		_text = "";
	}
}




