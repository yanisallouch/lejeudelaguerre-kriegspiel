using Godot;
using System;
using System.Collections.Generic;

/// <summary>
/// A Passive is a class that modifies all <see cref="Unit"/> at range (that can be targeted: <see cref="EntityFilter"/>). based on the type, a passive can have various effects on a unit 
/// </summary>
public abstract class Passive : Action {
    /// <summary>
    /// types of passives
    /// </summary>
    /// <param name="Emitter"> emits a radio signal </param>
    /// <param name="Relay"> if at range of a radio signal, emits a radio signal </param>
    /// <param name="Radio"> can only act if at range of a radio signal, surrenders otherwise </param>
    /// <param name="Buff"> enhance (or nerf) a unit </param>
    /// <param name="Protect"> units at range are immune </param>

    private static List<Passive> _passiveList = new List<Passive>();


    public int Priority {get; set;} = 0;

    public Passive (Passive passive, Unit parentUnit)
    : base (passive, parentUnit) {
        Priority = passive.Priority;
    }

    public override List<Unit> SelectRange () { return null; }

    /// <summary>
    /// Instantiates a Passive
    /// </summary>
    /// <param name="title"> the name of the passive </param>
    /// <param name="description"> the description of the passive </param>
    /// <param name="filter">  a filter list used to check if a passive can hit a target</param>
    /// <param name="shape"> the <see href="Shape"/> used to check if a target is at range</param>
    /// <param name="target"> the group of unit that can be targeted by the passive <see cref="EntityFilter.ETarget"/>.(All, Allies, Enemies, Noone) </param>
    /// <param name="type"> the type of passive. ( <see cref="EPassiveType"/>) </param>
    public Passive (String title, String description, List<String> filter, Shape shape, ETarget filterIgnore, List<String> blockedBy, ETarget blockedByIgnore)
    : base(title, description, filter, shape, filterIgnore, blockedBy, blockedByIgnore, null) {
        _passiveList.Add(this);
    }

    public void RmPassive () {
        _passiveList.Remove(this);
    }

    public abstract bool Config (Godot.Collections.Dictionary configDict);

    /// <summary>
    /// Returns the passive named "title"
    /// </summary>
    /// <param name="title"> the title of the passive to return</param>
    public static Passive GetPassive(String title) {
        Passive ans = null;

        foreach(Passive p in _passiveList) {
            if (p.GetTitle.Equals(title)) {
                ans = p;
                break;
            }
        }

        return ans;
    }

    public override String ToString () {
        String ans = base.ToString() + '\n';
        return ans;
    }

    public abstract Passive Clone (Unit parentUnit);

    // Called on all passives of the team at the beggining of its turn
    public abstract void Init ();

    // Called on all passives of the team after the init
    public abstract void Do ();

    ///TODO
    public static Passive StrToPassive (String type, String title, String description, List<String> filter, Shape shape, ETarget filterIgnore, List<String> blockedBy, ETarget blockedByIgnore) {
        switch (type) {
            case "Radio":
                return new Radio(title, description, filter, shape, filterIgnore, blockedBy, blockedByIgnore);
            default:
                return null;
        }
    }
}
