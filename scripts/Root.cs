using Godot;
using System;

public class Root : Node {
	public static Node RootNode;

	public override void _Ready() {
		RootNode = this;
	}
}
