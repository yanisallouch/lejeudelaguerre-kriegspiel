using Godot;
using System;

/// <summary>
/// An Entity is an abstract class that have a name and a description, it will mainly be used for display purposes
/// </summary>
public abstract class Entity : Spatial {
    private String _title;          public String GetTitle { get => _title; } public void SetTitle (String title) { _title = title; }
    private String _description;    public String GetDescription { get => _description; } public void SetDescription (String description) { _description = description; }
    
    public Entity (Entity entity) {
        _title = entity._title;
        _description = entity._description;
    }

    public Entity (String title, String description) {
        _title = title;
        _description = description;
    }

    public override String ToString () {
        String ans = "";
        ans += "Name: " + _title + '\n';
        ans += "Description: " + _description;
        return ans;
    }
}
