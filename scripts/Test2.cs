using Godot;
using System;

public class Test2 : Spatial {
	private PackedScene _unitScene;

	public override void _Ready()
	{
		CallDeferred("build");
	}

	private void build() {
		EntityBuilder builder = new EntityBuilder("Kriegsspiel", this.GetTree().GetRoot());
		
		// Console.WriteLine("##############");
		// Console.WriteLine();
		// foreach (Unit e in builder.GetUnitList) {
		//     //Console.WriteLine(e);
		//     //Console.WriteLine("");
		// }
		// Console.WriteLine("##############");
		// Console.WriteLine();

		Terrain.printTerrains();
		
		//Attaching one map to the scene
		Map b = builder.GetMapList[0];
		this.GetTree().GetRoot().AddChild(b);

		//Linking the button to the scenario (so the button can step the game forward)
		Node randomButton = GetNode("../2D/GUI/TopBar/RandomButton/Button");
		RandomButton random = (RandomButton)randomButton;
		random.SetScenario = builder.GetMapList[0].GetScenario;   
		
		Node button = GetNode("../2D/GUI/TopBar/ContinueButton/Button");
		NextButton nb = (NextButton)button;
		nb.SetScenario = builder.GetMapList[0].GetScenario;

		 
		
		Node leftText = GetNode("../2D/GUI/TopBar/LeftText/Value/Text/RichTextLabel");
		builder.GetMapList[0].GetScenario.SetLeftText = (RichTextLabel)leftText;
	}
}
